


default: /tmp/out.mp4

%.dvi: %.tex
	latex -interaction=batchmode -halt-on-error $<  > /dev/null

%.svg: %.dvi
	dvisvgm $< -n -v 0 -o $@ > /dev/null

/tmp/out.mp4: /usr/bin/js.s \
	main.js svg.js progress_bar.js \
	LaTeX.svg calculus.svg maxwell.svg
	/usr/bin/js.s -lm -lcairo -lmxml main.js
