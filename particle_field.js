
var radius = 2;

var radius2 = 10;

var G = 100 * 1000;

var F = 0.999;

var particle_field_tick = function()
{
	var cx = (this.cx = cos(time * 0.15) * width / 4 + width / 2);
	var cy = (this.cy = sin(time * 0.15) * height / 4 + height / 2);
	
	for (var i = 0, n = this.N; i < n; i++)
	{
		var me = this.particles[i];
		
		var ax = 0.0, ay = 0.0;
		
		var dist = hypot(cx - me.px, cy - me.py);
		if (dist > radius2)
		{
			ax += G * (cx - me.px) / (dist * dist * dist);
			ay += G * (cy - me.py) / (dist * dist * dist);
		}
		
		me.vx *= F, me.vy *= F;
		me.vx +=    ax * dt, me.vy +=    ay * dt;
		me.px += me.vx * dt, me.py += me.vy * dt;
		
//		debugger(me.px), debugger(me.py), sleep(1000);
	}
	
	this.happy = time > 140.0;
};


var particle_field_draw = function()
{
	cairo_set_source_rgb(cr, 1, 1, 1);
	
	cairo_arc(cr,
		xc = this.cx,
		yc = this.cy,
		radius = 2 * radius,
		angle1 = 0.0
		angle2 = M_TAU);
	
	cairo_stroke(cr);
	
	var max_velo = 0.0;
	
	for (var i = 0, n = this.N; i < n; i++)
	{
		var ele = this.particles[i];
		var velo = hypot(ele.vx, ele.vy);
		if (velo > max_velo)
			max_velo = velo;
	}
		
	for (var i = 0, n = this.N; i < n; i++)
	{
		var ele = this.particles[i];
		var velo = hypot(ele.vx, ele.vy);
		
		var q = velo / max_velo, q2 = 1.0 - q;
		
		cairo_set_source_rgb(cr,
			red   = q * 1.0 + q2 * 0.0
			green = q * 0.2 + q2 * 0.3
			blue  = q * 0.0 + q2 * 1.0);
		
		cairo_arc(cr,
			xc = ele.px,
			yc = ele.py,
			radius = radius,
			angle1 = 0.0
			angle2 = M_TAU);
		
		cairo_fill(cr);
	}
};

var particle_field = function(N)
{
	this.N = N;
	this.happy = false;
	
	this.cx = width / 2, this.cy = height / 2;
	
	var inc = 0.0;
	this.particles = [[0 ... N] := ({
		px: this.cx, py: this.cy,
		vx: cos(inc) * 20, vy: sin(inc++) * 20
	})];
	
	this.tick = particle_field_tick;
	this.draw = particle_field_draw;
};

// public attributes:
({
	particle_field: particle_field
});






















