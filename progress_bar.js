
var speed = M_TAU / 10;

var progress_bar_tick = function()
{
	if(this.progress >= M_TAU)
		this.happy = true;
	else
		this.progress += speed * dt;
};

var progress_bar_draw = function()
{
	cairo_set_line_width(cr, line_width);
	cairo_set_source_rgb(cr, this.red, this.green, this.blue);
	cairo_arc(cr,
		xc = width / 2,
		yc = height / 2,
		radius = this.radius,
		angle1 = 0.0
		angle2 = this.progress);
	cairo_stroke(cr);
};

var progress_bar_set_progress = function(new_progress)
{
	this.progress = new_progress;
	this.happy = false;
};

var progress_bar = function(r, g, b, radius)
{
	this.red = r, this.green = g, this.blue = b;
	this.radius = radius;
	this.happy = false;
	this.progress = 0.0;
	this.tick = progress_bar_tick;
	this.draw = progress_bar_draw;
	this.set_progess = progress_bar_set_progress;
};

// public attributes:
({
	progress_bar: progress_bar
});
