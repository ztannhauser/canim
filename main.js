

//var width = 256, height = 256, frame_rate = 1;
//var width = 512, height = 512, frame_rate = 1;
//var width = 1024, height = 1024, frame_rate = 1;
//var width = 1024, height = 1024, frame_rate = 10;
var width = 1024, height = 1024, frame_rate = 60;

var dt = 1.0 / frame_rate;

var surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, width, height);

var raw_pixel_data = cairo_image_surface_get_data(surface);

var cr = cairo_create(surface);

var line_width = width / 128;
cairo_set_line_width(cr, line_width);
cairo_set_line_cap(cr, CAIRO_LINE_CAP_ROUND);
cairo_set_line_join(cr, CAIRO_LINE_JOIN_ROUND);

var out_fd, ffmpeg_pid;

var output_path = "/tmp/out.mp4";

// setup ffmpeg connection:
{
	var pfd = [-1, -1];
	pipe(pfd);
	if(ffmpeg_pid = fork())
	{
		close(pfd[0]), out_fd = pfd[1];
	}
	else
	{
		close(pfd[1]), dup2(oldfd = pfd[0], newfd = stdin);
		dup2(oldfd = open("/dev/null", O_RDONLY), newfd = stderr); // no output
		execvp([
			"ffmpeg",
			"-y", // yes to overwite
			"-f", "rawvideo", // intput is raw pixels
			"-s", str(width) + "x" + str(height), // set resolution
			"-pix_fmt", "bgra", // set input format
			"-r", str(frame_rate), // set frame rate
			"-i", "-", // read from stdin
			"-an", // no audio
			output_path // pass through output video path
		]);
	}
}

var svg = import("svg.js");
var pb = import("progress_bar.js");
var pf = import("particle_field.js");

var svg_data_1 = svg.parse_svg("LaTeX.svg",
//	scale_factor = 16.0);
	scale_factor = 10.0);

var svg_data_2 = svg.parse_svg("calculus.svg",
//	scale_factor = 16.0);
	scale_factor = 8.0);

var svg_data_3 = svg.parse_svg("maxwell.svg",
//	scale_factor = 16.0);
	scale_factor = 8.0);

//debugger(svg_data);

//sleep(100);

var progress_bars = [];

var time = 0.0;

var tick = function()
{
	for (var i = 0, n = len(progress_bars); i < n; i++)
	{
		var ele = progress_bars[i];
		
		if(!ele.happy)
		{
			ele.tick();
		}
		
		ele.draw();
	}
	
	// Draw LaTeX:
	{
		var tx = 40, ty = 100, sx = 1, sy = 1;
		cairo_translate(cr, tx, ty);
		cairo_scale(cr, sx, sy);
		svg.draw(svg_data_1, time);
		cairo_scale(cr, 1.0 / sx, 1.0 / sy);
		cairo_translate(cr, -tx, -ty);
	}
	
	// Draw definition of derivitive:
	{
		var tx = 10, ty = height - 400, sx = 1, sy = 1;
		cairo_translate(cr, tx, ty);
		cairo_scale(cr, sx, sy);
		svg.draw(svg_data_2, time);
		cairo_scale(cr, 1.0 / sx, 1.0 / sy);
		cairo_translate(cr, -tx, -ty);
	}
	
	// Draw maxwell:
	{
		var tx = width - 600, ty = 100, sx = 1, sy = 1;
		cairo_translate(cr, tx, ty);
		cairo_scale(cr, sx, sy);
		svg.draw(svg_data_3, time);
		cairo_scale(cr, 1.0 / sx, 1.0 / sy);
		cairo_translate(cr, -tx, -ty);
	}
	
	time += dt;
	
	cairo_surface_flush(surface);
	write(out_fd, raw_pixel_data);
	memset(raw_pixel_data, 0);
};

var wait_for = function(seconds)
{
	for (var t = 0.0; t < seconds; t += dt)
	{
		tick();
	}
};

var wait = function()
{
	while(any(progress_bars, function(x){!x.happy} ))
	{
		tick();
	}
};


var first_radius  = width / 8;
var second_radius = first_radius  + line_width * 1.25;
var third_radius  = second_radius + line_width * 1.25;
var forth_radius  = third_radius  + line_width * 1.25;

wait_for(seconds = 10.0);

// particle field:
push(progress_bars, new (pf.particle_field)(N = 1000));

wait_for(seconds = 100.0);
//wait_for(seconds = 10.0);
//wait_for(seconds = 5.0);

push(progress_bars, new (pb.progress_bar)(r = 0.89, g = 0.19, b = 0, radius = first_radius));

wait_for(seconds = 1.0);

// the first and second:
push(progress_bars, new (pb.progress_bar)(r = 0.89, g = 0.79, b = 0, radius = second_radius));

wait_for(seconds = 1.0);

// the first, second and third:
push(progress_bars, new (pb.progress_bar)(r = 0.19, g = 0.79, b = 0, radius = third_radius));

wait_for(seconds = 1.0);

// all together now:
push(progress_bars, new (pb.progress_bar)(r = 0.19, g = 0.19, b = 0.79, radius = forth_radius));

wait();







// close output fd and wait for ffmpeg:
{
	var wstatus;
	close(out_fd);
	waitpid(ffmpeg_pid, &wstatus, 0);
	puts("ffmpeg's wstatus == " + str(wstatus));
}

puts("done. (video at '" + output_path + "'");









































