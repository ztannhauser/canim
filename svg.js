




// return operations:
var parse_path_d = function(d, scale_factor)
{
	var operations = [];
	
	var here = d, letter, there, params, diff;
	
	for (;here;here = there)
	{
		letter = here[0], here++;
		
		there = strpbrk(here, "MmZzLlHhVvCcSsQqTtAa");
		
		if (there)
		{
			diff = here - there;
			
			params = diff ? map(split(diff, " "), float) : [];
			
			params = map(params, function(x) {x * scale_factor} );
		}
		else
		{
			params = [];
		}
		push(operations, ({letter: letter, params: params}));
	}
	
	operations;
};

var parse_svg = function(path, scale_factor = 1.0)
{
	var svg_data = ({ });
	
	var tree = mxmlLoadFd(
		fd = open(path, O_RDONLY),
		cb = MXML_OPAQUE_CALLBACK);

	var svg = mxmlFindElement(
		node = tree,
		top = tree,
		element = "svg",
		descend = MXML_DESCEND);
	
	{
		var numbers = map(split(mxmlElementGetAttr(svg, "viewBox"), " "), float);
		svg_data.viewBox = ({
			("min-x"): numbers[0] * scale_factor,
			("min-y"): numbers[1] * scale_factor,
			("width"): numbers[2] * scale_factor,
			("height"): numbers[3] * scale_factor
		});
	}
	
	var definitions = (svg_data.definitions = ({
		paths: ({ })
	}));
	
	var defs = mxmlFindElement(svg, svg, element = "defs", descend = MXML_DESCEND);
	
	for (var child = mxmlGetFirstChild(defs); child;
		child = mxmlGetNextSibling(child))
	{
		if (mxmlGetType(child) == MXML_ELEMENT)
		{
			switch (mxmlGetElement(child))
			{
				case "path":
				{
					var d = mxmlElementGetAttr(child, "d");
					
					var id = mxmlElementGetAttr(child, "id");
					
					var operations = parse_path_d(d, scale_factor);
					
					definitions.paths[id] = operations;
				}
				
				default:
				{
					debugger(mxmlGetElement(child));
					puts("line 85");
					sleep(1000);
				}
			}
		}
	}
	
	var g = mxmlFindElement(tree, tree, element = "g", descend = MXML_DESCEND);
	
	puts("line 89");
	
	var group = (svg_data.group = []);
	
	for (var child = mxmlGetFirstChild(g); child;
		child = mxmlGetNextSibling(child))
	{
		if (mxmlGetType(child) == MXML_ELEMENT)
		{
			switch (mxmlGetElement(child))
			{
				case "use":
				{
					var x = float(mxmlElementGetAttr(child, "x"));
					
					var y = float(mxmlElementGetAttr(child, "y"));
					
					var href = mxmlElementGetAttr(child, "xlink:href")[1 ... ];
					
					push(group, ({
						action: "use",
						x: x * scale_factor,
						y: y * scale_factor,
						href: href,
					}));
				}
				
				case "rect":
				{
					var x = float(mxmlElementGetAttr(child, "x"));
					
					var y = float(mxmlElementGetAttr(child, "y"));
					
					var width = float(mxmlElementGetAttr(child, "width"));
					
					var height = float(mxmlElementGetAttr(child, "height"));
					
					push(group, ({
						action: "rect",
						x: x * scale_factor,
						y: y * scale_factor,
						width: width * scale_factor,
						height: height * scale_factor,
					}));
				}
				
				default:
				{
					debugger(mxmlGetElement(child));
					puts("line 85");
					sleep(1000);
				}
			}
		}
	}
	
	svg_data; // return 'ret'
};

var abs_moveto = function(params) // "M"
{
//	puts("abs_moveto");
	
	x = params[0], y = params[1];
	
	cairo_move_to(cr, x, y);
	
	for (var i = 2, n = len(params); i < n; i += 2)
	{
		var px = params[i + 0], py = params[i + 1];
		
		cairo_line_to(cr, px, py);
	}
};

var rel_moveto = function(params) // "m"
{
//	puts("rel_moveto");
	sleep(100);
};

var abs_lineto = function(params) // "L"
{
//	puts("abs_lineto");
	for (var i = 0, n = len(params); i < n; i += 2)
	{
		x = params[i + 0], y = params[i + 1];
		cairo_line_to(cr, x, y);
	}
};

var rel_lineto = function(params) // "l"
{
	puts("rel_lineto");
	sleep(100);
};

var abs_horizontal_lineto = function(params) // "H"
{
//	puts("abs_horizontal_lineto");
	for (var i = 0, n = len(params); i < n; i++)
	{
		x = params[i];
		cairo_line_to(cr, x, y);
	}
};

var rel_horizontal_lineto = function(params) // "h"
{
	puts("rel_horizontal_lineto");
	sleep(100);
};

var abs_vertical_lineto = function(params) // "V"
{
//	puts("abs_vertical_lineto");
	for (var i = 0, n = len(params); i < n; i++)
	{
		y = params[i];
		cairo_line_to(cr, x, y);
	}
};

var rel_vertical_lineto = function(params) // "v"
{
	puts("rel_vertical_lineto");
	sleep(100);
};

var last_x2 = 0, last_y2 = 0;

var abs_curveto = function(params) // "C"
{
//	puts("abs_curveto");
	
	for (var i = 0, n = len(params); i < n; i += 6)
	{
		var x1 = params[i + 0];
		var y1 = params[i + 1];
		var x2 = (last_x2 = params[i + 2]);
		var y2 = (last_y2 = params[i + 3]);
		var x3 = params[i + 4];
		var y3 = params[i + 5];
		
		cairo_curve_to(cr, x1, y1, x2, y2, x3, y3);
		x = x3, y = y3;
	}
};

var rel_curveto = function(params) // "c"
{
	puts("rel_curveto");
	sleep(100);
};

var abs_smooth_curveto = function(params) // "S"
{
//	puts("abs_smooth_curveto");
	
	for (var i = 0, n = len(params); i < n; i += 4)
	{
		var x1 = x - (last_x2 - x);
		var y1 = y - (last_y2 - y);
		var x2 = (last_x2 = params[i + 0]);
		var y2 = (last_y2 = params[i + 1]);
		var x3 = params[i + 2];
		var y3 = params[i + 3];
		
		cairo_curve_to(cr, x1, y1, x2, y2, x3, y3);
		x = x3, y = y3;
	}
};

var rel_smooth_curveto = function(params) // "s"
{
	puts("rel_smooth_curveto");
	sleep(100);
};

var closepath = function(params) // "Z" or "z"
{
	cairo_close_path(cr);
};

var operation_handlers = ({
	("M"): abs_moveto,
	("m"): rel_moveto,
	("L"): abs_lineto,
	("H"): abs_horizontal_lineto,
	("V"): abs_vertical_lineto,
	("C"): abs_curveto,
	("S"): abs_smooth_curveto,
	("Z"): closepath,
	("z"): closepath,
});

var draw_path = function(operations)
{
	var x = 0.0, y = 0.0; // pen position
	
	for (var i = 0, n = len(operations); i < n; i++)
	{
		var ele = operations[i];
		
		var handler = operation_handlers[ele.letter];
		
		if (handler)
			handler(ele.params);
		else
			sleep(100);
	}
//	cairo_stroke(cr);
	cairo_fill(cr);
};

var group_colors = 
[
	({red: 0.79, green: 0.34, blue: 0.23}),
	({red: 0.59, green: 0.74, blue: 0.23}),
	({red: 0.39, green: 0.74, blue: 0.23}),
	({red: 0.29, green: 0.54, blue: 0.79}),
	({red: 0.24, green: 0.34, blue: 0.73})
];

var draw = function(svg_data, time = 0.0)
{
	cairo_set_source_rgb(cr, 1, 1, 1);
	cairo_set_line_width(cr, 1);
	
	// draw viewBox:
	var vb = svg_data.viewBox;
	var tx = -vb."min-x", ty = -vb."min-y";
	
//	cairo_rectangle(cr, 0, 0, vb.width, vb.height), cairo_stroke(cr);
	
	cairo_translate(cr, tx, ty);
	// draw root group:
	{
		for(var i = 0, n = len(svg_data.group); i < n; i++)
		{
			var ele = svg_data.group[i];
			
			var color = group_colors[i % len(group_colors)];
			
			cairo_set_source_rgba(cr, color.red, color.green, color.blue, 0.7);
			
			switch(ele.action)
			{
				case "use":
				{
					var rad = (float(i) / n * M_TAU) + time;
					var scale = 5;
					var tx = ele.x + cos(rad) * scale, ty = ele.y + sin(rad) * scale;
					
					cairo_translate(cr, tx, ty);
					
					var path_operations = svg_data.definitions.paths[ele.href];
					
					draw_path(path_operations);
					
					cairo_translate(cr, -tx, -ty);
				}
				
				case "rect":
				{
					cairo_rectangle(cr, ele.x, ele.y, ele.width, ele.height);
					cairo_fill(cr);
				}
				
				default:
				{
					debugger(ele.action);
					sleep(100);
				}
			}
		}
	}
	
	cairo_translate(cr, -tx, -ty);
	
//	sleep(100);
};



// public attributes:
({
	parse_svg: parse_svg,
	draw: draw
});


























